<?php 

	$stars = BB_BRIDGE_URL . 'modules/bridge-testimonial/includes/five-stars.png';

?>

<div class="bb-bridge-testimonial">

	<?php if ( isset( $settings->person_image_src )): ?>	
		<div class="bb-bridge-testimonial-image">
			<img src="<?php echo $settings->person_image_src; ?>" alt="<?php echo esc_html($settings->person_name); ?>">
		</div>
	<?php endif; ?>

	<div class="bb-bridge-testimonial-wrap">
		<p><?php echo $settings->testimonial_text; ?></p>
		<?php if( $settings->testimonial_stars == 'yes' ): ?>
			<img src="<?php echo $stars; ?>" alt="rating stars">
		<?php endif; ?>
		<h4><?php echo esc_html($settings->person_name); ?></h4>
		<span><?php echo esc_html($settings->person_position); ?></span>
		<?php if ($settings->person_position AND $settings->person_company) { echo "-"; } ?>
		<a href= "<?php echo $settings->person_link; ?>"><?php echo esc_html($settings->person_company); ?></a>
	</div>

</div>